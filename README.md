**How to setup the project**


To setup this project, you need to have node installed on your system. To do this, go to your terminal and run "npm install".

Once that is done, clone or download this project to your local computer.

On a command line interface, navigate to the project folder and run "npm start".

If everything is setup appropriately, you should see the project live. Otherwise, try to debug any error that may surface. 
As errors may vary from person to person,I will recommend to visit the FAQ section of npm here: www.dummynpm.com/FAQ


We have attached the node.js LICENSE because our project is node based and we do not want any violations of copyright or IP

